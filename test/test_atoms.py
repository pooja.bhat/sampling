import pytest
from ase.build import molecule

from sampling.translate import (atoms_to_openbabel, atoms_to_rdkit,
                                rdkit_to_atoms)


def test_openbabel():
    atoms = molecule('CH2OCH2')
    mol = atoms_to_openbabel(atoms)
    assert mol.OBMol.NumAtoms() == len(atoms)
    assert mol.OBMol.NumBonds() == 7


def test_rdkit():
    atoms = molecule('C4H4NH')
    mol = atoms_to_rdkit(atoms)
    assert len(mol.GetAtoms()) == len(atoms)

    atoms2 = rdkit_to_atoms(mol)
    assert (atoms.symbols == atoms2.symbols).all()
    for a, a2 in zip(atoms, atoms2):
        assert a.position == pytest.approx(a2.position, 1.e-3)
