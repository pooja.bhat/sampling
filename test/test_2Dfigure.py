from rdkit import Chem
from rdkit.Chem import AllChem, Draw
from sampling.translate import AtomsCreator, atoms_to_rdkit


def test_2D():
    ac = AtomsCreator()
    atoms = ac.smiles('[nH]1cnc2cncnc21')
    #atoms.edit()
    mol = atoms_to_rdkit(atoms)
    AllChem.Compute2DCoords(mol)
    Draw.MolToFile(mol, 'withH.png')

    mol = atoms_to_rdkit(atoms)
    mol = Chem.RemoveHs(mol)
    Draw.MolToFile(mol, 'withoutH.png')
