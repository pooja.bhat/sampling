import pytest
from ase.build import molecule
from ase.optimize import BFGS

from sampling.pybel import Pybel
from sampling.optimize.internal import Internal


def test_energy_forces():
    atoms = molecule('CH2OCH2')

    atoms.calc = Pybel()
    E0 = atoms.get_potential_energy()

    opt = BFGS(atoms, trajectory='relax.traj')
    opt.run(fmax=0.01)
    assert E0 > atoms.get_potential_energy()


def test_relaxtion():
    formula = 'CH2OCH2'
    forcefield = 'uff'

    atoms0 = molecule(formula)
    atoms0.calc = Pybel(forcefield=forcefield)

    fmax = 0.05
    opt = BFGS(atoms0)
    opt.run(fmax=fmax)

    atoms1 = molecule(formula)
    atoms1.calc = Pybel(forcefield=forcefield)

    opt = Internal(atoms1)
    opt.run(fmax=fmax)

    assert atoms0.get_potential_energy() == pytest.approx(
        atoms1.get_potential_energy(), 1e-2)
