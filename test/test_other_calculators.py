import numpy as np
import pytest
from pathlib import Path
from ase.optimize import BFGS
from ase.calculators.dftb import Dftb

from sampling.translate import AtomsCreator
from sampling.conformers import Conformers


def test_dftb():
    atoms = AtomsCreator().smiles('CCCC')

    confs = Conformers(atoms)
    confs.add(1)
    confs.write('confs_initial.traj')

    confs.relax()
    confs.sort()
    confs.write('confs_mmff94.traj')

    def initialize(atoms):
        atoms.center(vacuum=3)
        atoms.calc = Dftb(
            slako_dir=str(
                Path(__file__).parent / 'testdata' / 'dftb') + '/')
        return atoms

    confs.relax(optimizer=BFGS, initialize=initialize)
    confs.write('confs_dftb.traj')

    E = np.array([conf.get_potential_energy() for conf in confs])
    confs.sort()
    Esort = np.array([conf.get_potential_energy() for conf in confs])
    assert (sorted(E) == Esort).all()


def test_torchani():
    try:
        from torchani.ase import Calculator as TorchANI
        from torchani.models import ANI1x
    except Exception:
        pytest.skip('Calculator not available')

    atoms = AtomsCreator().smiles('CCCC')

    confs = Conformers(atoms)
    confs.add(1)
    confs.write('confs_initial.traj')

    confs.relax()
    confs.sort()
    confs.write('confs_mmff94.traj')

    def initialize(atoms):
        atoms.center(vacuum=3)
        atoms.calc = TorchANI(species='HC', model=ANI1x())
        return atoms

    confs.relax(optimizer=BFGS, initialize=initialize)
    confs.write('confs_torchani.traj')

    E = np.array([conf.get_potential_energy() for conf in confs])
    confs.sort()
    Esort = np.array([conf.get_potential_energy() for conf in confs])
    assert (sorted(E) == Esort).all()
