========
Sampling
========

Project for the interaction between ASE and rdkit.

Install
=======

The package depends on the libraries
[openbabel]() and
[rdkit](https://www.rdkit.org/docs/Install.html).
See the installation instructions there.

We need python3 bindings to be able to work with ase.
In ubuntu::

  sudo apt install python3-openbabel
  sudo apt install python3-rdkit

Examples
========

There are calculators available in ASE style::

  from ase.build import molecule
  from sampling.mmff94 import MMFF94
  from sampling.pybel import Pybel

  atoms = molecule('CH4')

  atoms.calc = MMFF94()
  print('MMFF94 energy', atoms.get_potential_energy(), 'eV')

  from openbabel import pybel

  for ff in pybel.forcefields:
      atoms.calc = Pybel(forcefield=ff)
      print(ff.upper(), 'energy', atoms.get_potential_energy(), 'eV')

See an initial example for the creation of conformers in
doc/
