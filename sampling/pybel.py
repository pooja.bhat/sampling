import numpy as np
from openbabel import pybel

from ase.calculators.calculator import Calculator, all_changes
import ase.units as u

from sampling.translate import atoms_to_openbabel


class Pybel(Calculator):
    implemented_properties = ['energy', 'forces']
    default_parameters = {'forcefield': 'mmff94'}

    def calculate(
            self, atoms=None, properties=None,
            system_changes=all_changes):

        if properties is None:
            properties = self.implemented_properties

        Calculator.calculate(self, atoms, properties, system_changes)

        if atoms is None:
            atoms = self.atoms

        if 'numbers' in system_changes:
            self.mol = atoms_to_openbabel(atoms)
        elif 'positions' in system_changes:
            self.mol = atoms_to_openbabel(atoms)

        ff = pybel._forcefields[self.parameters['forcefield']]
        assert ff.Setup(self.mol.OBMol)
        self.results['energy'] = ff.Energy() * u.kcal / u.mol

        forces = np.zeros((len(atoms), 3))
        for ia, atom in enumerate(self.mol.atoms):
            grad = ff.GetGradient(atom.OBAtom)
            forces[ia] = [grad.GetX(), grad.GetY(), grad.GetZ()]
        self.results['forces'] = forces * u.kcal / u.mol

    def optimize(self, atoms, fmax, steps=None):
        """Relax internally

        atoms: atoms object
        fmax: maximal force (ignored)
        steps: maximal number of relaxation steps
        """
        assert len(atoms.constraints) == 0

        if steps is None:
            steps = 500  # pybel default

        self.get_potential_energy(atoms)

        self.mol.localopt(forcefield=self.parameters['forcefield'],
                          steps=steps)
        positions = np.zeros((len(atoms), 3))
        for ia, atom in enumerate(self.mol.atoms):
            positions[ia] = [
                atom.vector.GetX(), atom.vector.GetY(), atom.vector.GetZ()]
        atoms.positions = positions

        self.get_potential_energy(atoms)
