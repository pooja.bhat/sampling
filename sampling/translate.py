import tempfile
import numpy as np

from openbabel import pybel
from rdkit import Chem
from rdkit.Chem import AllChem

from ase import Atoms, Atom


def rdkit_to_atoms(mol, positions=None):
    """Create Atoms from rdkit mol

    mol: rdkit mol object
    positions: optionally give positions
    """
    na = len(mol.GetAtoms())

    if positions is None:
        conf = mol.GetConformer()
        positions = np.empty((na, 3))
        for ia in range(na):
            pos = conf.GetAtomPosition(ia)
            positions[ia] = [pos.x, pos.y, pos.z]
    else:
        positions = np.array(positions)
        assert positions.shape == (na, 3)

    atoms = Atoms()
    for rdatom, position in zip(mol.GetAtoms(), positions):
        atoms.append(Atom(rdatom.GetSymbol(), position))
    return atoms


def atoms_to_openbabel(atoms):
    # hack to get bonds
    with tempfile.NamedTemporaryFile(suffix='.xyz') as tf:
        atoms.write(tf.name)
        mol = next(pybel.readfile('xyz', tf.name))

    return mol


def openbabel_to_rdkit(mol):
    return Chem.MolFromMolBlock(mol.write('mol'), removeHs=False)


def atoms_to_rdkit(atoms):
    mol = atoms_to_openbabel(atoms)
    return openbabel_to_rdkit(mol)


class AtomsCreator:
    def smiles(self, smile_string):
        """Create atoms from a smiles string"""
        mol = Chem.MolFromSmiles(smile_string)
        mol = Chem.AddHs(mol)
        AllChem.EmbedMolecule(mol)#,boxDim=[200,200,200])
        return rdkit_to_atoms(mol)

    def rdkit(self, mol, positions=None):
        """Create atoms from a rdkit mol"""
        return rdkit_to_atoms(mol, positions)
