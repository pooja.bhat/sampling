import numpy as np

from ase.calculators.calculator import Calculator, all_changes
import ase.units as u

from rdkit.Chem import AllChem
from rdkit.Chem.rdForceFieldHelpers import (
    MMFFGetMoleculeProperties, MMFFGetMoleculeForceField)
from rdkit.Geometry import Point3D

from sampling.translate import AtomsCreator, atoms_to_rdkit


class MMFF94(Calculator):
    implemented_properties = ['energy', 'forces']

    def calculate(
            self, atoms=None, properties=None,
            system_changes=all_changes):

        if properties is None:
            properties = self.implemented_properties

        Calculator.calculate(self, atoms, properties, system_changes)

        if atoms is None:
            atoms = self.atoms

        if 'numbers' in system_changes:
            self.mol = atoms_to_rdkit(atoms)
        elif 'positions' in system_changes:
            conf = self.mol.GetConformer()
            for i in range(self.mol.GetNumAtoms()):
                conf.SetAtomPosition(i, Point3D(*atoms[i].position))

        mmffprops = MMFFGetMoleculeProperties(self.mol)
        ff = MMFFGetMoleculeForceField(self.mol, mmffprops)
        self.results['energy'] = ff.CalcEnergy() * u.kcal / u.mol
        forces = np.array(ff.CalcGrad()).reshape(len(atoms), 3)
        self.results['forces'] = - forces * u.kcal / u.mol

    def optimize(self, atoms, fmax, steps=None):
        """Relax internally

        atoms: atoms object
        fmax: maximal force (ignored)
        steps: maximal number of relaxation steps
        """
        assert len(atoms.constraints) == 0

        self.get_potential_energy(atoms)

        if steps is None:
            steps = 200  # rdkit default
        AllChem.MMFFOptimizeMolecule(self.mol, maxIters=steps)
        relaxed = AtomsCreator().rdkit(self.mol)
        atoms.set_positions(relaxed.get_positions())

        self.get_potential_energy(atoms)
