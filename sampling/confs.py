from rdkit.Chem import rdDistGeom
from rdkit.Chem import rdchem
from ase import io

from sampling.translate import AtomsCreator, atoms_to_rdkit
from sampling.mmff94 import MMFF94
from sampling.optimize.internal import Internal


class Conformers(list):
    def __init__(self, atoms):
        self.atoms = atoms
        self._mol = atoms_to_rdkit(atoms)
        super().append(atoms)

    def add(self, number, parameters={}):
        """Add a given number of conformers"""
        # see https://www.rdkit.org/docs/Cookbook.html#conformer-generation-with-etkdg
        settings = {
            'useSmallRingTorsions': True,
            #'randomSeed':0xf00d,
            'pruneRmsThresh': 0.5,
            'useRandomCoords': True,
            'boxDim' : [200, 200, 200],
            'boxSizeMult': 2.0,
            }
        settings.update(parameters)

        params = rdDistGeom.ETKDG()
        for key, value in settings.items():
            setattr(params, key, value)

        bounds = rdDistGeom.GetMoleculeBoundsMatrix(self._mol)
        a = rdchem.Conformer(self._mol.GetNumBonds())
        bonds = (self._mol.GetNumBonds())
        #num_atom = bonds+1
        #N = (num_atom-2)/7 #number of monomers
        #oxy = int(3*N-1)
        #print(bonds,N,oxy)
        #max_b = 0.82*bonds*1.464

        #bounds[0,oxy]
        #bounds[oxy,0]
        #print(bounds)
        params.SetBoundsMat(bounds)	
        cids = rdDistGeom.EmbedMultipleConfs(
            self._mol, numConfs=number, params=params)
        assert len(cids) == number, 'Conformer creation failed'

        for i, conf in enumerate(self._mol.GetConformers()):
            atoms = AtomsCreator().rdkit(self._mol,
                                         conf.GetPositions())
            super().append(atoms)

    def write(self, trajname):
        with io.Trajectory(trajname, 'w') as traj:
            for atoms in self:
                traj.write(atoms)

    def relax(self, fmax=0.05, optimizer=Internal,
              initialize=None):
        """Relax all conformers

        fmax: maximal force (ignored)
        optimizer: Optimizer to use, default Internal
        initialize: function to initialize the atoms object,
          must return the modified atoms object
        """
        for atoms in self:
            if initialize is None:
                atoms.calc = MMFF94()
            else:
                atoms = initialize(atoms)
            opt = optimizer(atoms)
            opt.run(fmax=fmax)

    def sort(self):
        """Sort conformers by energy"""
        super().sort(key=lambda atoms: atoms.get_potential_energy())
